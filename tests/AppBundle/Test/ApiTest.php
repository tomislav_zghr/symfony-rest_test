<?php

namespace Tests\AppBundle\Test;



use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


use AppBundle\Entity\Uri;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectRepository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\SchemaTool;
use Symfony\Component\HttpKernel\KernelInterface;


class ApiTest extends WebTestCase
{

    private function getRequest($type, $uri, array $data)
    {
        $content = $data;
        $client = static::createClient([
            'environment' => 'test',
            'debug'       => true
        ]);

        $client->request($type,  $uri, $content);

        return $client->getResponse();
    }

    private $content = array(
        'uri' => 'www.google.hr',
        'rating' => 2,
        'visitorId' => '8.8.8.8'
    );

    public function testCreate()
    {
        $content = $this->content;
        $response = $this->getRequest('put','/api/rate', $content);
        $jsonData = json_decode($response->getContent(), true);//var_dump($jsonData);var_dump($response->getContent());
        $this->assertEquals('success', $jsonData['status'], 'Uri not rated successfully');
        $this->assertEquals($content['rating'], $jsonData['rating'], "Inserted/updated rate doesn't match");
        $this->assertEquals($content['uri'], $jsonData['uri'], "URI doesn't match");
    }

    public function testUpdate()
    {
        $content = $this->content;
        $content['rating'] = 7;
        $response = $this->getRequest('put','/api/rate', $content);
        $jsonData = json_decode($response->getContent(), true);//var_dump($jsonData);var_dump($response->getContent());
        $this->assertEquals('success', $jsonData['status'], 'Uri rate not updated successfully');
        $this->assertEquals($content['rating'], $jsonData['rating'], "Inserted/updated rate doesn't match");
        $this->assertEquals($content['uri'], $jsonData['uri'], "URI doesn't match");
    }
    public function testRead()
    {
        $content = $this->content;
        $response = $this->getRequest('post', '/api/getrates', ['uri' => $content['uri']]);
        $jsonData = json_decode($response->getContent(), true);
        $this->assertEquals('success', $jsonData['status'], 'Cannot get uri rates sum');
        $this->assertEquals($content['uri'], $jsonData['uri'], "URI doesn't match");
    }

    public function testDelete()
    {
        $content = $this->content;
        $uri = urlencode($content['uri']);
        $visitorId = urlencode($content['visitorId']);
        $url = str_replace('%', '(+)', '/' . $uri . '/' . $visitorId);
        $response = $this->getRequest('delete', '/api/delete' . $url, $content);
        $jsonData = json_decode($response->getContent(), true);
        $this->assertEquals('success', $jsonData['status'], 'Cannot delete uri rate');
        $this->assertEquals($content['uri'], $jsonData['uri'], "URI doesn't match");
        $this->assertEquals(0, $jsonData['score'], "URI rate wasn't deleted");
    }
}