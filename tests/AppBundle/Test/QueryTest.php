<?php
// tests/AppBundle/Repository/ProductRepositoryTest.php
namespace Tests\AppBundle\Repository;

use AppBundle\Entity\Uri;
use AppBundle\Entity\UriRating;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductRepositoryTest extends KernelTestCase
{
    /**
    * @var \Doctrine\ORM\EntityManager
    */
    private $em;

    /**
    * {@inheritDoc}
    */
    protected function setUp()
    {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
        ->get('doctrine')
        ->getManager();
    }

    public function testsSumVisitorsAndScore()
    {
        $uriRatingSum = $this->em
        ->getRepository(UriRating::class)
        ->sumVisitorsAndScore(365)
        ;
        $this->assertCount(2, $uriRatingSum);
        $this->assertEquals(10, $uriRatingSum['sum_rating'], "sum_rating doesn't match");
        $this->assertEquals(1, $uriRatingSum['sum_users'], "sum_users doesn't match");
    }

    public function testsGetSumVisitorsAndScore()
    {
        $uriRatingSum = $this->em
            ->getRepository(Uri::class)
            ->getSumVisitorsAndScore('http://127.0.0.1:8000/tech')
        ;
        $this->assertCount(2, $uriRatingSum);
        $this->assertEquals(10, $uriRatingSum['sum_rating'], "sum_rating doesn't match");
        $this->assertEquals(1, $uriRatingSum['sum_users'], "sum_users doesn't match");
    }

    /**
    * {@inheritDoc}
    */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }
}