<?php

namespace AppBundle\Repository;


use AppBundle\AppBundle;
use AppBundle\Entity\UriRating;
use AppBundle\Entity\Uri;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Validation;



class UriRatingRepository extends EntityRepository
{
    private $resultData = [
        'status' => "failure",
        'uri' => "",
        'rating' => 0,
        'score' => 0
    ];



    private $errors = [];


    /**
     * @return array
     */
    public function getResultData()
    {
        return $this->resultData;
    }

    /**
     * @param array $resultData
     */
    public function setResultData($resultData)
    {
        $this->resultData = $resultData;
    }

     /**
     * @param array $columnFilter
     */
    public function setColumnFilter($request)
    {
        $visitorId = $request->get('visitorId');
        $uri = $request->get('uri');

        if($visitorId && $uri)
        {
            $this->columnFilter = [
                'visitorId' => $visitorId,
                'uri' => $uri
            ];
            return true;
        }
        else
        {
            return false;
        }
    }



    public function sumVisitorsAndScore($uri)
    {
        return $this->createQueryBuilder('ura')
            ->andWhere("ura.uri = '" . $uri . "'")
            ->select('COALESCE(SUM(ura.rating), 0) as  sum_rating, count(ura.visitorId) as sum_users')
            //->setParameters(['visitorId' => "'vv'"])
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
            //->getSql();
    }


    public function calculateScore($data)
    {
        if($data['sum_users'])
            $result = round($data['sum_rating'] / $data['sum_users'], 1);
        else
            $result = 0;
        return $result;
    }


    public function setErrors($errors)
    {
        foreach ($errors as $error) {
            $this->errors['errors'][] = $error->getMessage();
        }
    }
    public function getErrors()
    {
        return $this->errors;
    }


    public function validateObjects2($data, Request $request, $action)
    {

        $this->uriRating = new UriRating;
        if(!in_array($action, ['delete', 'get-rates']))
            $this->uriRating->setRating(filter_var($data['rating'], FILTER_SANITIZE_STRING));

        if(isset($data['visitorId']) && $data['visitorId'])
        {
            $this->uriRating->setVisitorId(filter_var($data['visitorId'], FILTER_SANITIZE_STRING));
        }
        else
        {
            $ip = $request->getClientIp();
            $this->uriRating->setVisitorId($ip);
        }

        $this->uri = new Uri();
        $this->uri->setUri(filter_var($data['uri'], FILTER_SANITIZE_STRING))
            ->addUriRating($this->uriRating)
        ;

        $validator = Validation::createValidator();
        $errors1 = $validator->validate($this->uriRating);
        $errors2 = $validator->validate($this->uri);

        var_dump((string)$errors2);
        if (count($errors1) > 0 || count($errors2) > 0) {

            $this->setErrors($errors1);
            $this->setErrors($errors2);
            return false;
        }
        else
        {
            return true;
        }
    }


}
