<?php

namespace AppBundle\Repository;
use Doctrine\ORM\EntityRepository;
use AppBundle\Entity\Uri;
use AppBundle\Entity\UriRating;


class UriRepository extends EntityRepository
{

    public function getSumVisitorsAndScore($uri)
    {

        $em = $this->getEntityManager();
        $uriData = $em->getRepository(Uri::class)
            ->findOneBy([
                'uri' => $uri
            ]);
        if($uriData) {
            $uriId = $uriData->getId();
        }
        else{
            $uriId = '0';
        }

        return $this->createQueryBuilder('u')
            ->andWhere("u.id = " . $uriId . "")
            //->setParameter('uri', $uri)
            ->select('u.sumRating as sum_rating, u.sumUsers as sum_users')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
            //->getSql();
    }

    public function updateRatingSums($data)
    {
        $em = $this->getEntityManager();

        $uri = $em->getRepository(Uri::class)
            ->findOneBy(['uri' => $data['uri']]);

        if (!$uri) {
            $uri = new Uri();
            $em->persist($uri);
        }

        $uri->setSumRating($data['sum_rating']);
        $uri->setSumUsers($data['sum_users']);
        $uri->setUri($data['uri']);


        $em->flush();
        return $uri;

    }




    public function insertUpdateDeleteUriRate(Uri $uri, UriRating $uriRating, $resultData, $action)
    {


        $uriRatingRep = $this
            ->getEntityManager()
            ->getRepository(UriRating::class);


        $em = $this->getEntityManager();

        $uriData = $em->getRepository(Uri::class)
            ->findOneBy([
                'uri' => $uri->getUri()
            ]);
        if (!$uriData) {

            if($action !== 'delete') {
                $uri->setSumUsers(0)
                    ->setSumRating(0);
                $em->persist($uri);
                $em->persist($uriRating);
                $em->flush();

                $sum = $uriRatingRep->sumVisitorsAndScore($uri->getId());
                $resultData['score'] = $uriRatingRep->calculateScore($sum);

                $uri->setSumUsers($sum['sum_users'])
                    ->setSumRating($sum['sum_rating']);

                $em->persist($uri);
                $em->flush();
            }

        }
        else
        {

            $uri->setId($uriData->getId());

            $uriRatingData = $em->getRepository(UriRating::class)
                ->findOneBy([
                    'visitorId' => $uriRating->getVisitorId(),
                    'uri' => $uri->getId()
                ]);

            if (!$uriRatingData) {

                $em->merge($uriRating);

            } else {

                $uriRating->setId($uriRatingData->getId());

                if($action == 'delete')
                    $em->remove($uriRatingData);
                else
                    $em->merge($uriRating);

            }

            $em->flush();

            $sum = $uriRatingRep->sumVisitorsAndScore($uri->getId());
            $resultData['score'] = $uriRatingRep->calculateScore($sum);
            $uri->setSumUsers($sum['sum_users'])
                ->setSumRating($sum['sum_rating']);


            if($sum['sum_users'] == 0)
                $em->remove($uriData);
            else
                $em->merge($uri);
            $em->flush();



        }

        return $resultData;

    }

}
