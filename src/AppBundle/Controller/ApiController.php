<?php

namespace AppBundle\Controller;
use AppBundle\Entity\UriRating;
use AppBundle\Entity\Uri;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;


class ApiController extends FOSRestController
{

    /**
     * @var Uri
     */
    private $uri;

    /**
     * @var UriRating
     */
    private $uriRating;

    /**
     * Object validation
     * @param $action string
     * @param $data array
     * @param Request $request
     * @return boolean
     */
    private function validateObjects($data, Request $request, $action)
    {
        $uriRatingRep = $this->getDoctrine()->getRepository(UriRating::class);
        $this->uriRating = new UriRating;
        if(!in_array($action, ['delete', 'get-rates']))
            $this->uriRating->setRating(filter_var($data['rating'], FILTER_SANITIZE_STRING));

        if(isset($data['visitorId']) && $data['visitorId'])
        {
            $this->uriRating->setVisitorId(filter_var($data['visitorId'], FILTER_SANITIZE_STRING));
        }
        else
        {
            $ip = $request->getClientIp();
            $this->uriRating->setVisitorId($ip);
        }

        $this->uri = new Uri();
        $this->uri->setUri(filter_var($data['uri'], FILTER_SANITIZE_STRING))
            ->addUriRating($this->uriRating)
        ;

        $validator = $this->get('validator');
        $errors1 = $validator->validate($this->uriRating);
        $errors2 = $validator->validate($this->uri);

        if (count($errors1) > 0 || count($errors2) > 0) {

            $uriRatingRep->setErrors($errors1);
            $uriRatingRep->setErrors($errors2);
            return false;
        }
        else
        {
            return true;
        }
    }


    /**
     * Update/Insert
     * @param ParamFetcher $paramFetcher
     * @param Request $request
     * @RequestParam(name="uri", requirements="(.){1,255}", nullable=false, allowBlank=false, strict=true, description="URI from which API call was made")
     * @RequestParam(name="rating", requirements="[1-9]|10", strict=true, nullable=false, allowBlank=false, description="URI rating sent via rating plugin")
     * @RequestParam(name="visitorId", requirements="(.){0,255}", allowBlank=true, strict=true, description="ID of the URI visitor can contain blank value or value of the hidden field for example")
     * @Rest\Put("/api/rate")
     * @return View()
     */
    public function updateInsertAction(ParamFetcher $paramFetcher, Request $request)
    {

        $uriRatingRep = $this->getDoctrine()->getRepository(UriRating::class);
        $resultData = $uriRatingRep->getResultData();
        $action = 'insert-update';
        try {
            $data = $paramFetcher->all();
            if ($this->validateObjects($data, $request, $action)) {
                $repoUri = $this->getDoctrine()->getRepository(Uri::class);
                $resultData = $repoUri->insertUpdateDeleteUriRate($this->uri, $this->uriRating, $resultData, $action);
                $resultData['uri'] = $this->uri->getUri();
                $resultData['rating'] = $this->uriRating->getRating();
            }
        }
        catch (BadRequestHttpException $error){
            $uriRatingRep->setErrors(array($error));
        }
        if(!empty($errors = $uriRatingRep->getErrors())) {
            $resultData['errors'] = $errors;
        }
        else {
            $resultData['status'] = 'success';
        }
        $view = $this->view($resultData);
        return $view;
    }



    /**
     * Delete
     * @Rest\Delete("/api/delete/{uri}/{visitorId}", requirements={"uri": "(.){1,255}", "visitorId": "(.){0,255}"})
     * @param string $uri
     * @param string $visitorId
     * @param Request $request
     * @return View()
     */
    public function deleteAction($uri, $visitorId, Request $request)
    {
        //var_dump($uri);
        //var_dump($visitorId);
//exit;
        $uri = urldecode(str_replace(array('(+)'), array('%'), filter_var($uri, FILTER_SANITIZE_STRING)));
        $visitorId = urldecode(str_replace(array('(+)'), array('%'), filter_var($visitorId, FILTER_SANITIZE_STRING)));
        $uriRatingRep = $this->getDoctrine()->getRepository(UriRating::class);
        $resultData = $uriRatingRep->getResultData();
        $action = 'delete';
        try {
            $data = ['uri' => $uri, 'visitorId' => $visitorId];
            if ($this->validateObjects($data, $request, $action)) {
                $repoUri = $this->getDoctrine()->getRepository(Uri::class);
                $resultData = $repoUri->insertUpdateDeleteUriRate($this->uri, $this->uriRating, $resultData, $action);
                $resultData['uri'] = $this->uri->getUri();
            }
        }
        catch (BadRequestHttpException $error){
            $uriRatingRep->setErrors(array($error));
        }
        if(!empty($errors = $uriRatingRep->getErrors())) {
            $resultData['errors'] = $errors;
        }
        else {
            $resultData['status'] = 'success';
        }
        $view = $this->view($resultData);
        return $view;

    }


    /**
     * Get URI rates
     * @Rest\Post("/api/getrates")
     * @RequestParam(name="uri", requirements="(.){1,255}", nullable=false, allowBlank=false, strict=true, description="URI rating sent via rating plugin")
     * @param ParamFetcher $paramFetcher
     * @param Request $request
     * @return View()
     */
    public function getRatesAction(ParamFetcher $paramFetcher, Request $request)
    {
        //var_dump($paramFetcher->all());
        //var_dump($request->request->all());

        $uriRatingRep = $this->getDoctrine()->getRepository(UriRating::class);
        $resultData = $uriRatingRep->getResultData();
        $action = 'get-rates';
        try {
            $data = $paramFetcher->all();
            if ($this->validateObjects($data, $request, $action)) {

                $sumRatings = $this->getDoctrine()
                    ->getRepository(Uri::class)
                    ->getSumVisitorsAndScore($this->uri->getUri());

                if (!empty($sumRatings))
                {
                    $resultData['score'] = $uriRatingRep->calculateScore($sumRatings);
                }
                $resultData['uri'] = $this->uri->getUri();
            }
        }
        catch (BadRequestHttpException $error){
            $uriRatingRep->setErrors(array($error));
        }
        if(!empty($errors = $uriRatingRep->getErrors())) {
            $resultData['errors'] = $errors;
        }
        else {
            $resultData['status'] = 'success';
        }
        $view = $this->view($resultData);
        return $view;

    }

}