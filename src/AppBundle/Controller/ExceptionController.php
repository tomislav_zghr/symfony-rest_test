<?php

namespace AppBundle\Controller;
//use Symfony\Bundle\TwigBundle\Controller\ExceptionController as BaseExceptionController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;

class ExceptionController extends Controller
{


    function __construct($debug) {

    }

    public function showExceptionAction(
        Request $request, FlattenException $exception, DebugLoggerInterface $logger = null
    )
    {
        if (is_numeric($exception->getStatusCode())) {

           /*return $this->redirectToRoute('error', [
               'code' => $exception->getStatusCode(),
               'error' => [$exception->getMessage()]
           ]);*/

            return $this->render('error.html.twig', [
                'status_code' => $request->get('code'),
                //'status_text' => $request->get('status_text'),
            ]);

        }
        return $this->get('twig.controller.exception')->showAction($request, $exception, $logger);
    }



}