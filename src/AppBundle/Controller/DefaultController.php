<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('home/index.html.twig', [
            'news' => 'homepage',
        ]);
    }
    /**
     * @Route("/sport", name="sport")
     */
    public function sportAction(Request $request)
    {
        return $this->render('sport/index.html.twig', [
            'news' => 'sport',
        ]);
    }
    /**
     * @Route("/tech", name="tech")
     */
    public function techAction(Request $request)
    {
        return $this->render('tech/index.html.twig', [
            'news' => 'culture',
        ]);
    }

    /**
     * @Route("/*", name="all")
     */
    public function allAction(Request $request)
    {
        return $this->render('base.html.twig', [
            'news' => 'home',
        ]);
    }

    /**
     * @Route("/error/{code}", name="error")
     */
    public function errorAction(Request $request)
    {//var_dump($request);
        return $this->render('error.html.twig', [
            'status_code' => $request->get('code'),
            //'status_text' => $request->get('status_text'),
        ]);
    }

}
