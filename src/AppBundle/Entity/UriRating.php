<?php

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * UriRating
 * @UniqueEntity(fields={"visitorId", "uri"}, message="It looks like your index exists!")
 */

class UriRating
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "Field Visitor ID must be at least {{ limit }} characters long",
     *      maxMessage = "Field Visitor ID cannot be longer than {{ limit }} characters"
     * )(groups={"data-validation"})
     * @Assert\Type("string")
     */
    private $visitorId;



    /**
     * @var int
     * @Assert\Type("integer")
     * @Assert\Range(
     *      min = 0,
     *      max = 10,
     *      minMessage = "Rating must be between 0 and 10",
     *      maxMessage = "Rating must be between 0 and 10"
     * )
     */
    private $rating;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set id
     *
     * @param integer $id
     *
     * @return UriRating
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set visitorId
     *
     * @param string $visitorId
     *
     * @return UriRating
     */
    public function setVisitorId($visitorId)
    {
        $this->visitorId = $visitorId;

        return $this;
    }

    /**
     * Get visitorId
     *
     * @return string
     */
    public function getVisitorId()
    {
        return $this->visitorId;
    }



    /**
     * Set rating
     *
     * @param integer $rating
     *
     * @return UriRating
     */
    public function setRating($rating)
    {
        $this->rating = (int)$rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }



    /**
     * @var \AppBundle\Entity\Uri
     */
    private $uri_rating;


    /**
     * Set uriRating
     *
     * @param \AppBundle\Entity\Uri $uriRating
     *
     * @return UriRating
     */
    public function setUriRating(\AppBundle\Entity\Uri $uriRating = null)
    {
        $this->uri_rating = $uriRating;

        return $this;
    }

    /**
     * Get uriRating
     *
     * @return \AppBundle\Entity\Uri
     */
    public function getUriRating()
    {
        return $this->uri_rating;
    }
    /**
     * @var \AppBundle\Entity\Uri
     */
    private $uri;


    /**
     * Set uri
     *
     * @param \AppBundle\Entity\Uri $uri
     *
     * @return UriRating
     */
    public function setUri(\AppBundle\Entity\Uri $uri = null)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return \AppBundle\Entity\Uri
     */
    public function getUri()
    {
        return $this->uri;
    }
}
