<?php

namespace AppBundle\Entity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Uri
 * UniqueEntity(fields={"uri"}, message="It looks like uri already exists!")
 */

class Uri
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\NotNull()
     * @Assert\Length(
     *      min = 1,
     *      max = 255,
     *      minMessage = "Field uri must be at least {{ limit }} characters long",
     *      maxMessage = "Field uri cannot be longer than {{ limit }} characters"
     * )
     * @Assert\Type("string")
     */
    private $uri;

    /**
     * @var int
     * @Assert\Type("integer")
     */
    private $sumRating;

    /**
     * @var int
     * @Assert\Type("integer")
     */
    private $sumUsers;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $uri_rating;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param integer $id
     *
     * @return Uri
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set uri
     *
     * @param string $uri
     *
     * @return Uri
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set sumRating
     *
     * @param integer $sumRating
     *
     * @return Uri
     */
    public function setSumRating($sumRating)
    {
        $this->sumRating = $sumRating;

        return $this;
    }

    /**
     * Get sumRating
     *
     * @return int
     */
    public function getSumRating()
    {
        return $this->sumRating;
    }

    /**
     * Set sumUsers
     *
     * @param integer $sumUsers
     *
     * @return Uri
     */
    public function setSumUsers($sumUsers)
    {
        $this->sumUsers = $sumUsers;

        return $this;
    }

    /**
     * Get sumUsers
     *
     * @return int
     */
    public function getSumUsers()
    {
        return $this->sumUsers;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->uri_rating = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add uriRating
     *
     * @param \AppBundle\Entity\UriRating $uriRating
     *
     * @return Uri
     */
    public function addUriRating(\AppBundle\Entity\UriRating $uriRating)
    {

        $uriRating->setUri($this);

        $this->uri_rating[] = $uriRating;

        return $this;
    }

    /**
     * Remove uriRating
     *
     * @param \AppBundle\Entity\UriRating $uriRating
     */
    public function removeUriRating(\AppBundle\Entity\UriRating $uriRating)
    {
        $this->uri_rating->removeElement($uriRating);
    }

    /**
     * Get uriRating
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUriRating()
    {
        return $this->uri_rating;
    }
}
