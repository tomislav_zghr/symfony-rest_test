$.fn.ratePlugin = function(parameters) {



    var plugin_url = 'http://127.0.0.1:8000';
    var plugin_id = 'web-rating';
    var min_rating = 1;
    var max_rating = 5;
    var rating_html = '<p>Rate article:</p><div id="rating-score-total" class="rating-score-total">N/A</div>';
    rating_html += '<div class="rating rating-score-holder">';

    var thisElement = $(this);
    function getJson(apiPath, type, data, func)
    {
        $.ajax({
            url: plugin_url + apiPath,
            type: type,
            data: data,
            cache: false,
            async: true,
            success: function(result){//alert(result.toSource());
                console.log(result);
                func(result);
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR.responseText);
                console.log(textStatus);
                console.log(errorThrown);
                func(false);
            }
        });
    }

    function getVisitorId(parameters)
    {
        if(parameters.user_id_field)
            return $('#' + parameters.user_id_field).val();
        else
            return '';
    }

    function sanitizeUrl(url)
    {
        url = url.replace(new RegExp('\%', 'g'), '(+)');
        return url;
    }

    function selectScore(jsonData)
    {
        thisElement.find('div#rating-score-total').html(jsonData.score);
        var intScore = Math.round(jsonData.score);
        thisElement.find('.rating .selected').removeClass('selected');
        thisElement.find('label#score-' + intScore).addClass('selected');
    }
    var data = {};
    data['uri'] = window.location.href;

    getJson('/api/getrates' , 'post', data, function (jsonData) {

        if(jsonData && jsonData.status && jsonData.status == 'success') {
            selectScore(jsonData);
        }
    });


    if(thisElement[0]) {
        if(Number(parameters.maxrating) == parameters.maxrating)
            max_rating = parameters.maxrating;
        for(var cnt = max_rating; cnt >= min_rating; cnt--)
            rating_html += '<label id="score-' + cnt + '"><input type="radio" name="rating" value="' + cnt + '" title="' + cnt + ' stars">' + cnt + '</label>'
        rating_html += '</div><span id="rating-clear" class="rating-clear"><i class="fa fa-times fa-1 " aria-hidden="true"></i></span><div class="rating-score-info" id="' + plugin_id + '-score"></div>';
        thisElement.html(rating_html);
    }


    thisElement.find('.rating input').change(function (e) {
        var radio = $(this);


        var data = {};
        data['visitorId'] = getVisitorId(parameters);
        data['rating'] = parseInt(radio.val(), 10);
        data['uri'] = window.location.href;

        radio.prop('checked', false);
        getJson('/api/rate', 'put', data, function (jsonData) {

            if(jsonData && jsonData.status && jsonData.status == 'success') {
                selectScore(jsonData);
                thisElement.find('span#rating-clear').css('display', 'inline-block');
            }
        });


    }).parent('label').mouseover(function () {
        var radio = $(this).find('input[type="radio"]');
        thisElement.find('div#' + plugin_id + '-score').css('display', 'inline-block').html(radio.val());
    }).mouseleave(function () {
        thisElement.find('div#' + plugin_id + '-score').hide().html('');
    });

    thisElement.on('click', 'span#rating-clear', function(e) {
        e.preventDefault();

        var data = {};
        data['visitorId'] = getVisitorId(parameters);
        data['uri'] = window.location.href;
        var urlString = sanitizeUrl('/' + encodeURIComponent(data['uri']) + '/' + encodeURIComponent(data['visitorId']));
        getJson('/api/delete' + urlString, 'delete', data, function (jsonData) {

            if(jsonData && jsonData.status && jsonData.status == 'success') {
                selectScore(jsonData);
                thisElement.find('span#rating-clear').css('display', 'none');
            }
        });
    });



};
