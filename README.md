# Symfony REST service with jQuery URI rating plugin
## 1. Symfony service
### 1.1 Setup

Git clone
```sh
> git clone https://tomislav_zghr@bitbucket.org/tomislav_zghr/symfony-rest_test.git
```
After Git repository is cloned we need to install Symfony 
```sh
> composer install
```

Composer will read composer.json and install Symfony wit all required bundles

At the end of the instalation you will be asked to enter database and email server parameters
```sh
database_host (127.0.0.1): localhost
database_port (null): 3306
database_name (symfony): symfony-rest_test
database_user (root): someuser
database_password (null): somepass
mailer_transport (smtp):
mailer_host (127.0.0.1):
mailer_user (null):
mailer_password (null):
secret (ThisTokenIsNotSoSecretChangeIt): blahblahblah
```

I installed these bundles in this project:

```sh
friendsofsymfony/rest-bundle
jms/serializer-bundle
nelmio/cors-bundle
symfony/assetic-bundle
```

Crate the database (doctrine will read parameters.yml and crate the databasa)
```
php bin/console doctrine:database:create
```

Now we can update the schema to create the tables
```sh
php bin/console doctrine:schema:update --force
```
Doctrine configuration:

#######UriRating.yml
```yml
AppBundle\Entity\UriRating:
    type: entity
    table: null
    repositoryClass: AppBundle\Repository\UriRatingRepository
    id:
        id:
            type: integer
            id: true
            generator:
                strategy: AUTO
    fields:
        visitorId:
            type: string
            length: '255'
            column: visitor_id
        rating:
            type: integer
    lifecycleCallbacks: {  }
    manyToOne:
        uri:
            targetEntity: Uri
            inversedBy: uri_rating
            joinColumn:
                name: uri_id
                referencedColumnName: id

```
#######Uri.yml
```yml
AppBundle\Entity\UriRating:
AppBundle\Entity\Uri:
    type: entity
    table: null
    repositoryClass: AppBundle\Repository\UriRepository
    id:
        id:
            type: integer
            id: true
            generator:
                strategy: AUTO
    fields:
        uri:
            type: string
            length: 255
            unique: true
        sumRating:
            type: integer
            column: sum_rating
        sumUsers:
            type: integer
            column: sum_users
    lifecycleCallbacks: {  }
    oneToMany:
        uri_rating:
            targetEntity: UriRating
            mappedBy: uri

```

Entities are related together
UriRating many to one Uri
Uri One to many UriRating

###API

I created controller for the sample news website, service API and exception for custom errors

Website controller routes:

 * homepage
 * sport
 * tech

Api controller routes

 * /api/rate
 * /api/getrates
 * /api/delete

Exception controller

 * displaying custom error pages

####API routes

##### /api/rate route:

Purpose: inserting/updating uri rates into uri_rate table.
After UriRate is updated/inserted Uri will be updated with sum of rates and visitors

Rest configuration:

    /**
     * Update/Insert
     * @param ParamFetcher $paramFetcher
     * @param Request $request
     * @RequestParam(name="uri", requirements="(.){1,255}", nullable=false, allowBlank=false, strict=true, description="URI from which API call was made")
     * @RequestParam(name="rating", requirements="[1-9]|10", strict=true, nullable=false, allowBlank=false, description="URI rating sent via rating plugin")
     * @RequestParam(name="visitorId", requirements="(.){0,255}", strict=true, description="ID of the URI visitor can contain empty value or value of the hidden field for example")
     * @Rest\Put("/api/rate")
     * @return View()
     */
We are using RequestParameters that will allow us to validate $paramFetcher values

* HttpRequest we are using in this route is PUT
* uri
 - requires string value with length from 1 to 255 characters
 - cannot be null (parametter must be passed to API)
 - cannot be blank
* rating
 - requires number from 1-10
 - cannot be null (parametter must be passed to API)
 - cannot be blank
* visitorID
 - requires string value with length from 0 to 255 characters
 - cannot be null (parametter must be passed to API)
 - value can be blank

All REST exceptions will be stored into array and returned in json response

##### /api/delete route:

Purpose: deleting uri rating for specific visitorId and Uri.
After UriRate is deleted Uri will be updated with sum of rates and visitors of remaining data or if no remaining data left Uri will be deleted

Rest configuration:

    /**
     * Delete
     * @Rest\Delete("/api/delete/{uri}/{visitorId}", requirements={"uri": "(.){1,255}", "visitorId": "(.){0,255}"})
     * @param string $uri
     * @param string $visitorId
     * @param Request $request
     * @return View()
     */
We are using RequestParameters that will allow us to validate $paramFetcher values

* HttpRequest we are using in this route is DELETE
* uri
 - requires string value with length from 1 to 255 characters
 - cannot be null (parametter must be passed to API)
 - cannot be blank
* visitorID
 - requires string value with length from 0 to 255 characters
 - cannot be null (parametter must be passed to API)
 - value can be blank

All REST exceptions will be stored into array and returned in json response


##### /api/getrates route:

Purpose: getting average Uri score sum of rates devided with sum of visitorIds .

Rest configuration:

    /**
     * Get URI rates
     * @Rest\Post("/api/getrates")
     * @RequestParam(name="uri", requirements="(.){1,255}", nullable=false, allowBlank=false, strict=true, description="URI rating sent via rating plugin")
     * @param ParamFetcher $paramFetcher
     * @param Request $request
     * @return View()
     */
We are using RequestParameters that will allow us to validate $paramFetcher values

* HttpRequest we are using in this route is POST
* uri
 - requires string value with length from 1 to 255 characters
 - cannot be null (parametter must be passed to API)
 - cannot be blank

All REST exceptions will be stored into array and returned in json response


Each API will return json array

Returns JSON array:

```json
{
  "status": "failure|failure",
  "uri": "some_uri",
  "rating": 3, 
  "score": 4.12,
  "errors": {}
}
```

* rating: (will be 0 on /api/delete and /api/getrates request)
* errors: (optional) - only if REST exception occurred)

# URI rating jQuery plugin
````````
I created 2 test pages for testing.
Web page uses bootstrap CSS and JS to display responsive layout.


######Plugin requirements:
* jQuery 2+
* font-awsome 3+
```html
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
```

* rating.js - will be placed on server (user will include script link to client web page)

To activate plugin method ratePlugin needs to be assigned to a div element

Example:
```js
$(document).ready(function () {
    $('div#web-rating-plugin').ratePlugin(
        {
            maxrating: 10,
            user_id_field: 'test-ip'
        }
    );
});
```

* maxrating
 - user can enter number between 2 - 10 (if this parameter is not defined default value will be 5)
* user_id_field
 - user defines id of the input field which contains VisitorID

Plugin will display URI score, rating stars (number depending on maxrating parameter).

* URI score
 - (average user score) visible in separate frame left from rating starts
* Rating stars
 - changing colors folloving first star to the last one hovering by mouse
 - if URI has already been rated stars will display rounded URI score to 0 decimals (if score is 5.6 then 6 stars will be selected)
* Delete button
 - After visitor rates the URI small X will appear on right side of the rating stars
 - If visitor clicks on it DELETE request willl be sent and if rate is deleted successfully X will disappear
* Rating stars score value display
 - when visitor hovers the star with mouse on right side small framed number will apear displaying current star score value
 - when user moves mouse from star framed number will disappear

Visitor can rate URI, update the vote and unvote (delete) it.

#PHPUnit tests

####API test
Contains 4 testing methods each testing values of json response (presuming no existing rates for same URI)

1. Testing URI rate create (/api/rate) PUT request
2. Testing URI rate update (/api/rate) PUT request (changed rate)
3. Testing URI rate read (/api/getrates) POST request
4. Testing URI rate deleted (/api/delete) DELETE request

####Query test
Contains 2 methods

1. Testing summing rate and visitors
2. Testing reading sums of reatings and visitors
